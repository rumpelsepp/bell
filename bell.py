#!/usr/bin/env python3

import argparse
import time
from enum import Enum
from typing import List, Optional

from periphery import GPIO
from libinput import ContextType, LibInput
from libinput.constant import KeyState
from libinput.event import KeyboardEvent


class Light:

    def __init__(self, pin_no: int) -> None:
        self.gpio = GPIO(pin_no, "out")
        self.state = False

    def __del__(self) -> None:
        self.gpio.close()

    def toggle(self) -> None:
        if self.state is True:
            self.state = False
        else:
            self.state = True
        self.gpio.write(self.state)


class Beeper:

    def __init__(self, pin_no: int) -> None:
        self.gpio = GPIO(pin_no, "out")

    def __del__(self) -> None:
        self.gpio.close()

    def _beep(self, sleep: float) -> None:
        self.gpio.write(True)
        time.sleep(sleep)
        self.gpio.write(False)

    def beep(self) -> None:
        self._beep(0.1)

    def beep_long(self) -> None:
        self._beep(0.5)

    def beep_error(self) -> None:
        for _ in range(5):
            self.beep()
            time.sleep(0.05)

    def beep_success(self) -> None:
        for _ in range(2):
            self.beep_long()
            time.sleep(0.1)


class States(Enum):
    IDLE = 0
    CODE_INPUT = 1
    LIGHT_INPUT = 2
    LOCKED = 3


class MatrixReader:
    keymap = {
        0x200: "0",
        0x201: "1",
        0x202: "2",
        0x203: "3",
        0x204: "4",
        0x205: "5",
        0x206: "6",
        0x207: "7",
        0x208: "8",
        0x209: "9",
        0x20a: "*",
        0x20b: "#",
    }

    def __init__(self, keyboard: str, code: List[str], light_trigger: str,
                 beeper: Beeper, light: Light):
        self.code = code
        self.lock_for = 60
        self.light_trigger = light_trigger
        self.light_trigger_time = 0.5
        self.beeper = beeper
        self.light = light
        self.libinput = LibInput(context_type=ContextType.PATH)
        self.libinput.add_device(keyboard)

        self._error_counter = 0
        self._locked_time = 0.0
        self._code_buffer: List[str] = []
        self._state = States.IDLE
        self._light_triggered_at: Optional[float] = None

    def _reset(self, beep: bool = True) -> None:
        if beep:
            time.sleep(0.2)
            self.beeper.beep_error()
        self._code_buffer = []
        self._state = States.IDLE

    def handle_light_trigger(self, event: KeyboardEvent) -> None:
        if event.key_state == KeyState.PRESSED:
            self._light_triggered_at = time.time()
        elif event.key_state == KeyState.RELEASED:
            if self._light_triggered_at is not None \
                    and time.time() - self._light_triggered_at > self.light_trigger_time:
                self.beeper.beep_long()
                self.light.toggle()
            self._state = States.IDLE

    def handle_code_input(self, event: KeyboardEvent, key: str):
        if event.key_state != KeyState.PRESSED:
            return
        if not (key.isdigit() or key == "#"):
            print(f"state machine bug {self._state}")
            self._reset()
        self.beeper.beep()
        print(self._code_buffer)

        if key == "#":
            if self._code_buffer == self.code:
                time.sleep(0.4)
                self.beeper.beep_success()
                # TODO: Open door.
                print("door is open")
                self._reset(False)
            else:
                self._error_counter += 1
                self._reset()
                if self._error_counter > 3:
                    self._state = States.LOCKED
                    self._locked_time = time.time()
        else:
            if len(self._code_buffer) > 16:
                self._reset()
            self._code_buffer.append(key)

    def run(self) -> None:
        for event in self.libinput.events:
            if not event.type.is_keyboard():
                continue
            if event.key not in self.keymap:
                print(f"unrecognized keycode {event.key}")
                continue

            key = self.keymap[event.key]

            # TODO: Use a state machine pattern that state
            # transations are more clear than here.
            if self._state == States.IDLE:
                if key == self.light_trigger:
                    self.handle_light_trigger(event)
                    self._state = States.LIGHT_INPUT
                else:
                    self.handle_code_input(event, key)
            elif self._state == States.CODE_INPUT:
                self.handle_code_input(event, key)
            elif self._state == States.LIGHT_INPUT:
                self.handle_light_trigger(event)
            elif self._state == States.LOCKED:
                if time.time() - self._locked_time > self.lock_for:
                    self._state = States.CODE_INPUT
                    self._error_counter = 0
                    self.handle_code_input(event, key)
                    continue
                self.beeper.beep_error()
            else:
                raise RuntimeError("unknown state")

            print(f"{event.key_state.name}: {key}")


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-k",
        "--keyboard",
        default="/dev/input/event0",
        help="Path to the keyboard device file",
    )
    parser.add_argument(
        "-l",
        "--light-pin",
        type=int,
        default=5,
        help="Pin number of the light",
    )
    parser.add_argument(
        "-b",
        "--beeper-pin",
        type=int,
        default=16,
        help="Pin number of the piezo beeper",
    )
    parser.add_argument(
        "-c",
        "--code",
        required=True,
        help="The doorcode",
    )
    return parser.parse_args()


def main() -> None:
    args = parse_args()
    beeper = Beeper(args.beeper_pin)
    light = Light(args.light_pin)
    mr = MatrixReader(args.keyboard, list(args.code), "*", beeper, light)
    mr.run()


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        pass
