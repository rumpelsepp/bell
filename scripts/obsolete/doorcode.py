#!/usr/bin/env python3

import os
import os.path
import signal
import sys
import threading
import time

import dbus
import dbus.service
import dbus.mainloop.glib
from gi.repository import GLib


CODE = os.getenv('DOORCODE_CODE')
DURATION = os.getenv('DOORCODE_RELAY_DURATION', '1')
TIMEOUT = int(os.getenv('DOORCODE_TIMEOUT', '6'))

DBUS_INTERFACE = 'org.residenz.Bell'


class DBusInterface(dbus.service.Object):
    DBUS_NAME = 'org.residenz.PinReader'
    DBUS_OBJECT_PATH = '/gra/residenz/PinReader'

    def __init__(self):
        self.bus = dbus.SessionBus()
        bus_name = dbus.service.BusName(self.DBUS_NAME, bus=self.bus)
        super().__init__(bus_name, self.DBUS_OBJECT_PATH)

    @dbus.service.signal(DBUS_INTERFACE)
    def pin_entered(self):
        print('success')

    @dbus.service.signal(DBUS_INTERFACE)
    def denied(self):
        print('denied')

    @dbus.service.signal(DBUS_INTERFACE)
    def bruteforce_detected(self):
        print('brute force detected, sleeping')

    @dbus.service.signal(DBUS_INTERFACE)
    def bruteforce_released(self):
        print('released')


class DBusWorker(threading.Thread):

    def __init__(self, interface, loop):
        self.interface = interface
        self.loop = loop
        super().__init__()

    def run(self):
        self.loop.run()

    def stop(self):
        self.loop.quit()


def now():
    return time.clock_gettime(time.CLOCK_MONOTONIC)


class PINReader:

    def __init__(self, dbus_interface):
        self.dbus_interface = dbus_interface
        self.error_counter = 0
        self.last_error = 0

    def read_pin(self):
        userinput = ''
        first = True

        for char in sys.stdin:
            # set timeout
            if first:
                print('read started, setting timeout')
                signal.alarm(TIMEOUT)
                first = False

            char = char.strip()

            # hashtag triggers comparison
            if char == '#':
                if userinput == CODE:
                    dbus_iface.pin_entered()
                else:
                    dbus_iface.denied()
                    self.error_counter += 1
                    self.last_error = now()

                break

            elif char == '*':
                print('aborted')
                break

            if len(userinput) > 32:
                print('too many characters')
                self.error_counter += 1
                self.last_error = now()
                break

            userinput += char

        signal.alarm(0)

    def run(self):
        # There are two loops. If the inner one fails, the state
        # is reset and the dance starts again.
        while True:
            self.error_counter = 0
            self.last_error = 0

            while True:
                if self.error_counter >= 3 and now() - self.last_error <= 10:
                    dbus_iface.bruteforce_detected()

                    time.sleep(10)
                    dbus_iface.bruteforce_released()
                    break

                try:
                    self.read_pin()
                except TimeoutError:
                    print('timed out, reset state')
                    break


def sigalarm_handler(signum, stackframe):
    print('SIGALRM fired')
    raise TimeoutError


# Create DBus interface and run it in the background.
# Signals on UNIX are emitted only to the main thread.
# The SIGALRM handler should not disturb the DBus worker.
dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
dbus_iface = DBusInterface()
dbus_loop = GLib.MainLoop()
dbus_worker = DBusWorker(dbus_iface, dbus_loop)


def main():
    if CODE is None:
        print('please set environment DOORCODE_CODE')
        sys.exit(1)

    signal.signal(signal.SIGALRM, sigalarm_handler)
    dbus_worker.start()

    pin_reader = PINReader(dbus_iface)
    pin_reader.run()


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        dbus_worker.stop()
