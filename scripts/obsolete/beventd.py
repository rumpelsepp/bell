from pydbus import SessionBus
from gi.repository import GLib


class Bell:
    """
    <node>
        <interface name='gra.residenz.Bell'>
            <method name='Hello'/>
        </interface>
    </node>
    """

    def Hello(self):
        print("Hello, World!")


bus = SessionBus()
bus.publish("gra.residenz.Bell", Bell())

loop = GLib.MainLoop()
loop.run()
