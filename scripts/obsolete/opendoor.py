#!/usr/bin/env python3

import argparse
import time
import os

import dbus
import dbus.service
import dbus.mainloop.glib
from gi.repository import GLib


DBUS_INTERFACE = 'org.residenz.Bell'

class DBusInterface(dbus.service.Object):
    DBUS_NAME = 'org.residenz.GPIOProxy'
    DBUS_OBJECT_PATH = '/gra/residenz/GPIOProxy'

    def __init__(self):
        self.bus = dbus.SessionBus()
        bus_name = dbus.service.BusName(self.DBUS_NAME, bus=self.bus)
        super().__init__(bus_name, self.DBUS_OBJECT_PATH)

    @dbus.service.method(DBUS_INTERFACE)
    def open_door():
        print('door open!!')


def gpio_path(pin):
    return '/sys/class/gpio/gpio%s' % pin


def gpio_init(pin):
    basepath = gpio_path(pin)

    with open('/sys/class/gpio/export', 'w') as f:
        f.write(str(pin))

    with open(os.path.join(basepath, 'direction'), 'w') as f:
        f.write('out')

    with open(os.path.join(basepath, 'value'), 'w') as f:
        f.write('1')


def gpio_deinit(pin):
    basepath = gpio_path(pin)

    with open('/sys/class/gpio/unexport', 'w') as f:
        f.write(str(pin))


def gpio_pulse(pin, duration):
    basepath = gpio_path(pin)

    with open(os.path.join(basepath, 'value'), 'w') as f:
        f.write('0')

    time.sleep(duration)

    with open(os.path.join(basepath, 'value'), 'w') as f:
        f.write('1')


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i',
        '--init',
        action='store_true',
        help='init GPIO foo',
    )
    parser.add_argument(
        '-d',
        '--deinit',
        action='store_true',
        help='deinit GPIO foo',
    )
    parser.add_argument(
        '-p',
        '--pin',
        type=int,
        required=True,
        help='relay pin number',
    )
    parser.add_argument(
        '-t',
        '--time',
        type=int,
        default=1,
        help='time in sec for relay current',
    )
    args = parser.parse_args()

    if args.init:
        gpio_init(args.pin)
    elif args.deinit:
        gpio_deinit(args.pin)
    else:
        gpio_pulse(args.pin, args.time)


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        pass
