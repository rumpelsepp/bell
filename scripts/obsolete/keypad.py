#!/usr/bin/env python3

import evdev
from evdev import InputDevice, categorize


def main():
    dev = InputDevice('/dev/input/by-path/platform-matrix-keypad-event')
    dev.grab()

    keymap = {
        0x200: '0',
        0x201: '1',
        0x202: '2',
        0x203: '3',
        0x204: '4',
        0x205: '5',
        0x206: '6',
        0x207: '7',
        0x208: '8',
        0x209: '9',
        0x20a: '*',
        0x20b: '#',
    }

    for event in dev.read_loop():
        if event.type != evdev.ecodes.EV_KEY:
            continue

        data = evdev.categorize(event)

        # We discard release events.
        if data.keystate == 1:
            continue

        key = keymap.get(data.scancode, '?')
        print(key, flush=True)


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        pass
